# Changes in 2.0.0 (unreleased)

## Breaking changes

- remove the config module. All configuration is now done via environment variables.
- Identifiers which do not produce results will produce an error.

## Fixes

- search: now also looks through integers
- search: output the full hierarchy (instead of first and last element)

## Notes

- lookup environment variables for the editor `VICONTEXT_EDITOR` and `VISUAL` (and `EDITOR` default vim)
- search: add tests
- search: specify `--rendered` to search through the fully rendered config context.
- search: optionally only search key `--key` or value `--value`.
- allow opening multiple contexts at the same time by specifying multiple identifiers, globs,
  or combinations of the two.

# Changes in 1.1.0

- search: the output and method has been refined to search all keys, values and print the full
  hierachy.

## New features

- implement `--search` option

# Changes in 1.0.0

initial release
