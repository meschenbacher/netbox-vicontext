#!/usr/bin/env python3

# Requirements:
# python packages
# pynetbox pyyaml
# env variables
# VICONTEXT_CONFIG_MODULE EDITOR

import pynetbox
import sys, os
import yaml
import tempfile
import subprocess
import difflib
import argparse
import importlib
from copy import deepcopy
from fnmatch import fnmatch

parser = argparse.ArgumentParser(description="""Edit and search netbox config context for
devices, virtual machines, and config contexts. Configuration via NETBOX_URL, NETBOX_TOKEN,
EDITOR (see README for more information).""")
group = parser.add_mutually_exclusive_group()
group.add_argument('--confirm', '-c', action='store_true', help='Show a diff and confirm changes.')
group.add_argument('--diff', '-d', action='store_true', help='Show a diff.')
parser.add_argument('--search', '-s', default='', type=str, help='Search through all config contexts (global, device and vm) for the string in one of key or value.')
parser.add_argument('--key', '-k', default='', type=str, help='Search through all config contexts (global, device and vm) for the string in the key.')
parser.add_argument('--value', '-v', default='', type=str, help='Search through all config contexts (global, device and vm) for the string in the value.')
parser.add_argument('--rendered', '-r', action='store_true', help='Use the fully rendered config context when searching (only affects devices and virtual machines).')
parser.add_argument('identifiers', nargs='*', type=str, help='Specify one or more globs of netbox devices, virtual machines or config contexts')

EDITOR = os.environ.get('VICONTEXT_EDITOR', os.environ.get('VISUAL', os.environ.get('EDITOR', 'vim')))

# use a custom dumper to increase indentation introducing new blocks
class IndentationDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super().increase_indent(flow, False)

def edit(edits):

    td = tempfile.TemporaryDirectory()
    for edition in edits:
        name = edition['device'].name
        edition['tf'] = open(os.path.join(td.name, name+'.yml'), 'w+', encoding='utf-8')
        tf = edition['tf']
        tf.write(edition['context'])
        tf.flush()

    subprocess.call([EDITOR, *[ e['tf'].name for e in edits ]])

    for edition in edits:
        edition['tf'].seek(0)
        edition['context'] = edition['tf'].read()
        edition['tf'].close()
    td.cleanup()

    return edits


def _listsearch(l: list, k: str, v: str) -> (list, bool):
    ret = []
    for i in l:
        if not i:
            pass
        elif isinstance(i, dict):
            match, thisfound = _dictsearch_helper(i, k, v, i)
            if thisfound:
                ret.append(match)
        elif isinstance(i, list):
            match, thisfound = _listsearch(i, k, v)
            if thisfound:
                ret.append(match)
        elif v and v in str(i):
            ret.append(i)
    return ret, len(ret) > 0


def _dictsearch_helper(d: dict, k: str, v: str, state: dict) -> (dict, bool):
    """
    recursion helper including the path
    return state and if a match has been found
    """
    found = False

    # we use a shallow copy of d because we're editing the state[key] during iteration
    for key, value in dict(d).items():

        roundfound = False
        keymatch = False

        # look in the key
        if k and k in str(key):
            roundfound = True
            keymatch = True
            state[key] = None

        # look in the value
        if not value:
            pass
        elif isinstance(value, dict):
            match, thisfound = _dictsearch_helper(value, k, v, value)
            if thisfound:
                state[key] = match
                roundfound = True
            elif keymatch:
                state[key] = {}
        elif isinstance(value, list):
            match, thisfound = _listsearch(value, k, v)
            if thisfound:
                state[key] = match
                roundfound = True
            elif keymatch:
                state[key] = []
        elif v and v in str(value):
            roundfound = True
            state[key] = value
        elif keymatch:
            state[key] = None

        if not roundfound:
            if state.get(key):
                del state[key]
        else:
            found = True

    return state, found

def dictsearch(d: dict, key: str, value: str) -> dict:
    """
    search through key and values in a dict and return the match including full hierarchy as
    well as all children (including values).
    """
    return _dictsearch_helper(deepcopy(d), key, value, {})[0]


def command_search(nb, args):
    matches = []
    key, value = args.search, args.search
    if args.key:
        key = args.key
    if args.value:
        value = args.value
    for i in nb.extras.config_contexts.all():
        if hasattr(i, 'data'):
            r = dictsearch(i.data, key, value)
            if len(r) > 0:
                print(yaml.dump({i.name: r}))
    for i in list(nb.dcim.devices.all()) + list(nb.virtualization.virtual_machines.all()):
        prop = 'local_context_data'
        if args.rendered:
            prop = 'config_context'
        if hasattr(i, prop) and getattr(i, prop):
            r = dictsearch(getattr(i, prop), key, value)
            if len(r) > 0:
                print(yaml.dump({i.name: r}))


def command_edit(nb, args):
    devices = list()
    vms = list()
    contexts = list()
    device = None

    cdevices = list(nb.dcim.devices.all())
    cvms = list(nb.virtualization.virtual_machines.all())
    ccontexts = list(nb.extras.config_contexts.all())
    for ident in args.identifiers:

        # we'd like to output an error when a identifier yields no results
        found = False

        for device in cdevices:
            if fnmatch(device.name, ident):
                found = True
                devices.append(device)

        for vm in cvms:
            if fnmatch(vm.name, ident):
                found = True
                vms.append(vm)

        for context in ccontexts:
            if fnmatch(context.name, ident):
                found = True
                contexts.append(context)

        if not found:
            print("Identifier", ident, 'yielded no results', file=sys.stderr)
            exit(1)

    count = len(devices) + len(vms) + len(contexts)
    if count > 1:
        print(devices, vms, contexts)
        ans = input("Opening {} files. [O]k".format(count))
        if ans and ans not in ('O', 'o'):
            exit(1)

    edits = []
    for device in devices + vms + contexts:
        if hasattr(device, 'local_context_data'):
            # devices and virtual machines use this attribute
            if device.local_context_data is None:
                # prevent a fresh context from showing as
                # null
                # ...
                # in the editor
                initial_context = ""
            else:
                initial_context = device.local_context_data
        elif hasattr(device, 'data'):
            # config contexts use this attribute
            if device.data is None:
                initial_context = ""
            else:
                initial_context = device.data
        initial_context = yaml.dump(initial_context, Dumper=IndentationDumper)
        edits.append(dict(initial_context=initial_context, context=initial_context,
                                  device=device))

    while True:
        edits = edit(edits)
        retry = False
        founddiff = False
        for edition in edits:
            name = edition['device'].name
            context = edition['context']
            try:
                yaml.load(context, Loader=yaml.FullLoader)
            except Exception as e:
                print("Error parsing yaml:", e, file=sys.stderr)
                ans = input("Edit or quit [E/q]?")
                if ans in ["q", "Q"]:
                    sys.exit(1)
                else:
                    retry = True

            if args.confirm or args.diff:
                lines = list(difflib.unified_diff(
                    edition['initial_context'].splitlines(keepends=True),
                    context.splitlines(keepends=True),
                    fromfile='a/'+name+'.yml', tofile='b/'+name+'.yml',
                ))
                if len(lines) == 0:
                    continue
                sys.stdout.writelines(lines)
                founddiff = True

        if founddiff and args.confirm:
            ans = input("Confirm save [Y/n] (ctrl-c to quit)?")
            if ans not in ["", "Y", "y"]:
                retry = True

        if retry:
            continue

        # now save all contexts
        for edition in edits:
            context = edition['context']
            # this may return None in case of an empty file and thats exactly what
            # netbox also does
            yamldata = yaml.load(context, Loader=yaml.FullLoader)
            if hasattr(edition['device'], 'local_context_data'):
                edition['device'].local_context_data = yamldata
            elif hasattr(edition['device'], 'data'):
                edition['device'].data = yamldata
            edition['device'].save()
        # finally done
        break


def main(nb, args):
    if args.search or args.key or args.value:
        if args.identifiers:
            print('identifiers may only be used for editing', file=sys.stderr)
            exit(1)
        if args.diff:
            print('--diff option may only be used for editing', file=sys.stderr)
            exit(1)
        if args.confirm:
            print('--confirm option may only be used for editing', file=sys.stderr)
            exit(1)
        command_search(nb, args)
    else:
        if args.rendered:
            print('--rendered option may only be used for searching', file=sys.stderr)
            exit(1)
        command_edit(nb, args)


if __name__ == '__main__':
    args = parser.parse_args()
    nb = pynetbox.api(os.environ['NETBOX_URL'], os.environ['NETBOX_TOKEN'])
    main(nb, args)
