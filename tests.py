import unittest
import random
import subprocess, io
from vicontext import dictsearch


class TestDictsearch(unittest.TestCase):

    def test_empty(self):
        self.assertEqual(dictsearch({}, ""), {})

    def test_search(self):
        """
        test matchin of keys and values and check that the complete hierarchy is output
        """
        d = dict(
                foo=dict(bar=dict(baz='bazbaz', test='baztest')),
                second=dict(third=dict(fourth=dict(fifth='sixth', seventh='eigthth'))),
        )
        for i in ['foo', 'oo', 'azba', 'ztes', 'bar']:
            self.assertEqual(dictsearch(d, i), dict(foo=dict(bar=dict(baz='bazbaz',
                                                                          test='baztest'))))
        for i in ['eigthth', 'thth', ]:
            self.assertEqual(dictsearch(d, i),
                             dict(second=dict(third=dict(fourth=dict(fifth='sixth',
                                                                     seventh='eigthth')))))
        self.assertEqual(dictsearch(d, "o"), d)
        d = dict(
                foo=dict(bar=dict(baz='bazbaz', test='baztest')),
                second=dict(third=dict(fourth=dict(fifth='sixth', seventh='eigthth'))),
                third={4242: dict(numbers=1337)},
        )
        self.assertEqual(dictsearch(d, 4242), dict(third={4242: dict(numbers=1337)}))
        self.assertEqual(dictsearch(d, 1337), dict(third={4242: dict(numbers=1337)}))
        self.assertEqual(dictsearch(d, "337"), dict(third={4242: dict(numbers=1337)}))
        self.assertEqual(dictsearch(d, "t"), d)


if __name__ == '__main__':
    unittest.main()
