# netbox vicontext

Edit (and search through) netbox config contexts with your favourite editor (chosen via
environment variables `VICONTEXT_EDITOR` over `VISUAL` over `EDITOR` over vim).

For editing and when multiple identifiers are supplied or matched (as they are interpreted as
globs), the editor is called for each of the file as `$EDITOR $FILE1...`.

```
usage: vicontext.py [-h] [-c | -d] [--search SEARCH] [--key KEY] [--value VALUE] [--rendered] [identifiers ...]
```

`vicontext.py` honors/requires the following environment variables

- `NETBOX_URL` (required)
- `NETBOX_TOKEN` (required)
- `VICONTEXT_EDITOR` over `VISUAL` over `EDITOR` (default `vim`)
- variables from [pynetbox](https://github.com/netbox-community/pynetbox), which in turn uses
  [python-requests](https://github.com/psf/requests), e.g. `http_proxy https_proxy no_proxy
  REQUESTS_CA_BUNDLE`, etc.

# install

```
virtualenv -p python3 venv
pip install -r requirements.txt
```

# example usage

```
# edit a context
python vicontext.py machine1.example.org

# edit a group context
python vicontext.py Debian

# edit multiple contexts with globs and a list
python vicontext.py www\?.example.org Debian dmz

# search through all contexts, keys, values and pring the full hierarchy of each match
python vicontext.py --search foo
```

As alias (with absolute paths)

```
alias vicontext="NETBOX_URL=https://netbox.example.org NETBOX_TOKEN='|echo foobar' ~/path/to/netbox-vicontext/venv/bin/python ~/path/to/netbox-vicontext/vicontext.py"
```
